

var vu = new Vue({
  el:"#TestApp",
  delimiters:["{*", "*}"],
  data:{
    cards:[],
    choosenCards:[],
    curPos:0,
    colors:[],
    sets:"",
    optionsOpened:false
  },

  computed:{
    cardList: function(){
      return this.cards.slice(this.curPos, this.curPos + 18)
    },
  },
  methods:{
    addCard(card){
      var found = false;
      if(this.choosenCards.length!=0)
        this.choosenCards.forEach((item, i, arr)=>{
          if(item.card.id == card.id){
            arr[i].count = item.count + 1;
            found = true;
          }});
      if(!found)
        this.choosenCards.push({card:card, count:1});
    },
    nextPage :function(){
      this.curPos = this.curPos + 18;
    },
    prevPage :function(){
      if(this.curPos - 18 >= 0)
        this.curPos = this.curPos - 18;
      else
        this.curPos = 0;
    },

  },
  watch:{
    colors:function(){
      var request = {};
      request["colors"] = this.colors.join(',');
      request["set"] =  this.sets;
      curPos = 0;
      this.cards = [];
      upDateCards(request, this.cards);
    },
    sets:function(){
      var request = {};
      request["colors"] = this.colors.join(',');
      request["set"] = this.sets;
      curPos = 0;
      this.cards = [];
      upDateCards(request, this.cards);
    }
  }
});

function upDateCards(request, cardsObj){
  var urlRequest = "https://api.magicthegathering.io/v1/cards?";
  Object.keys(request).forEach((value) => {
    urlRequest = urlRequest.concat(value+"="+request[value]+"&");
  });
  urlRequest.slice(0, -1);

  fetch(urlRequest)
      .then((response) => {
          if(response.ok) {
              return response.json();
          }
          throw new Error('Network response was not ok');
      })
      .then((json) => {
        json.cards.forEach(element=>{
          if("imageUrl" in element)
            cardsObj.push(element);
        })
      })
      .catch((error) => {
          console.log(error);
      });
}
